package ca.polymtl.log4420
package snippet

import net.liftweb._
import util.Helpers._

import xml.NodeSeq
import model.ComiteProgramme

object Programmes extends SnippetDB
{
  def render: NodeSeq => NodeSeq =
  {
    ".programme *" #> programmes.map
    {
      case( comite, cheminements ) =>
      ".nom *" #> comite.genie &
      ".cheminement *" #> cheminements.map(
      c =>
      {
        "a *" #> c.toString &
        "a [href]" #> Cheminement.menuComite.calcHref(c)
      })

    }

  }
}