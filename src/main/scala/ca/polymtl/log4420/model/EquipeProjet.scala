package ca.polymtl.log4420.model

case class Membre( firstName: String, lastName: String, email: String )

object EquipeProjet
{
  val members = List(
    Membre("Guillaume","Massé","guillaume.masse@polymtl.ca"),
    Membre("Guillaume","Massé","guillaume.masse@polymtl.ca"), // <- lol
    Membre("Guillaume","Massé","guillaume.masse@polymtl.ca"),
    Membre("Guillaume","Massé","guillaume.masse@polymtl.ca")
  )
}