package ca.polymtl.log4420
package test.systeme

import org.scalatest.{BeforeAndAfterEach, FunSpec, Suite}
import fixture.ProgrammeDB


trait DbMock extends FunSpec with BeforeAndAfterEach
{ this: Suite =>

  override def afterEach()
  {
    ProgrammeDB.reset()
  }
}