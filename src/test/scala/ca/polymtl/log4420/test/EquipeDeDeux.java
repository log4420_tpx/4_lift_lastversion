package ca.polymtl.log4420.test;

import java.lang.annotation.*;
import org.scalatest.TagAnnotation;

@TagAnnotation
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface EquipeDeDeux{
    public int pointsDeux();
    public int pointsQuatre();
}