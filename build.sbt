name := "tp4 lift"

version := "0.0.1"

organization := "ca.polymtl.log4420"

scalaVersion := "2.9.2"

seq( webSettings :_* )

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
	val liftVersion = "2.5-M2"
	Seq(
		"net.liftweb"				%% "lift-webkit"		    % liftVersion           % "compile",
		"org.eclipse.jetty"	 		 %  "jetty-webapp"			% "7.5.4.v20111024"     % "container; test",
		"ch.qos.logback"	 		 %  "logback-classic"		% "1.0.6",
		"org.scalatest"     		%% "scalatest"              % "2.0.M4"              % "test",
		"org.seleniumhq.selenium" 	 % "selenium-server" 		% "2.9.0"
  	)
}

port in container.Configuration := 8080

parallelExecution in Test := false